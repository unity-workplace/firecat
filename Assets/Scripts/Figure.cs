﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Figure : MonoBehaviour
{
    public int maxHP;
    public int HP;
    public float speed;
    public int direction=1;
    
    public virtual void takeDamage(int Damage)
    {
        HP -= Damage;
        if (HP <= 0)
        {
            Destroy(gameObject);
        }
    }
    protected void setDirection(int movement)
    {
        setDirection((float)movement);
    }
    protected void setDirection(float movement)
    {
        if (movement < 0)
        {
            if (direction == 1)
            {
                direction = -1;
                transform.localScale = new Vector3(direction * Mathf.Abs(transform.localScale.x), transform.localScale.y);
            }
        }
        else if (movement > 0)
        {
            if (direction == -1)
            {
                direction = 1;
                transform.localScale = new Vector3(direction * Mathf.Abs(transform.localScale.x), transform.localScale.y);
            }
        }

    }
}
