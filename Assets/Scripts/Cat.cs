﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cat : Company
{
    override protected void Follow()
    {
        Vector2 dist =  rider.position- transform.position;
        if (dist.magnitude> 1.5f)
        {
            transform.Translate(dist*0.2f * speed);
            setDirection(dist.x);
        }
    }
}
