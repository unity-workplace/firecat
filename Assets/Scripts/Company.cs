﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Company : Figure
{
    public Transform rider;
    public Player player;
    public bool following;

    protected void Start()
    {
        direction = 1;
        player = rider.GetComponent<Player>();
        following = true;
    }
    protected void Update()
    {
        if (following)
        {
            Follow();
        }
    }
    protected virtual void Follow()
    {
        
    }
    
}
