﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Figure
{
    protected int mana;
    public bool ground;
    Rigidbody2D rb;
    protected void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    protected virtual void Update()
    {
        
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            setDirection(-1);

            transform.Translate(Vector2.right * Input.GetAxis("Horizontal") * speed);
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            setDirection(1);
            transform.Translate(Vector2.right * Input.GetAxis("Horizontal") * speed);

        }
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (ground)
            {
                ground = false;
                rb.velocity = new Vector2(rb.velocity.x, 4);
            }
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            QSpell();
        }
        
    }
    
    protected virtual void QSpell()
    {


    }
    protected void OnCollisionStay2D(Collision2D collision)
    {
        ground = true;
    }
}
