﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mage : Player
{
    public Transform fireball;
    override protected void QSpell()
    {
        Missile ball;
        //Instantiate(fireball, transform.position, Quaternion.identity);
        ball= Instantiate(fireball, transform.position+Vector3.right*direction, Quaternion.identity).GetComponent<Missile>();
        ball.direction = direction;
    }
}
