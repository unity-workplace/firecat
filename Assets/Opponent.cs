﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Opponent : Figure
{
    public SpriteRenderer[] Eyes;
    public Color eyeColor;
    private void Start()
    {
        foreach (SpriteRenderer eye in Eyes)
        {
            eye.color = new Color(eyeColor.r, eyeColor.g, eyeColor.b);
        }
    }
    public override void takeDamage(int Damage)
    {
        base.takeDamage(Damage);
        foreach (SpriteRenderer eye in Eyes)
        {
            eye.color = new Color(eyeColor.r, eyeColor.g, eyeColor.b, (float)HP/maxHP);
        }
    }
}
