﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : Figure
{
    protected virtual void Start()
    {
    }

    void Update()
    {
        transform.Translate(Vector3.right * direction*speed);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Opponent")
        {
            collision.GetComponent<Figure>().takeDamage(10);
            Destroy(gameObject);

        }
    }
}
